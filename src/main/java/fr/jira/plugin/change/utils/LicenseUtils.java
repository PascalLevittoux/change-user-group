package fr.jira.plugin.change.utils;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.plugin.Plugin;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.PluginLicense;
import com.atlassian.upm.api.util.Option;

public class LicenseUtils {

	public static String getLicenseError(PluginLicenseManager pluginLicenseManager) {

		Plugin plugin = ComponentAccessor.getPluginAccessor().getPlugin(pluginLicenseManager.getPluginKey());
		String pluginVersion = "<b>" + plugin.getPluginInformation().getVersion() + "</b>";
		String error = null;

		Option<PluginLicense> license = pluginLicenseManager.getLicense();
		if (license.isDefined()) {
			PluginLicense pluginLicense = license.get();

			if (license.isDefined()) {
				if (pluginLicense.getError().isDefined()) {
					switch (license.get().getError().get()) {
						case EXPIRED:
							error = "Invalid license: Your evaluation license of " + plugin.getName()
									+ " expired. Please use the 'Buy' button to purchase a new license.";
							break;
						case TYPE_MISMATCH:
							error = "Invalid license";
							break;
						case VERSION_MISMATCH:
							error = "Invalid license: Your license for maintenance of " + plugin.getName() + " is not valid for version " + pluginVersion
									+ ". Please use the 'Renew' button to renew your " + plugin.getName() + " license.";
							break;
					}

				} else {
					// handle valid license scenario
				}
			} else {
				error = plugin.getName() + " is unlicensed. Please use the 'Buy' button to purchase a new license.";
			}
		} else {
			error = plugin.getName() + " is unlicensed. Please use the 'Buy' button to purchase a new license.";
		}
		return error;

	}

	public static boolean isLicenseValid(PluginLicenseManager pluginLicenseManager) {
		// If license exist - check it


		Option<PluginLicense> l = pluginLicenseManager.getLicense();
		if (l.isDefined())
			return l.get().isValid();


		return false;
	}

}
