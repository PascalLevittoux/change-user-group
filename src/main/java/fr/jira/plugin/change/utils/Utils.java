package fr.jira.plugin.change.utils;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Created by dom on 18.02.2017.
 */
public class Utils {
    public static void removeUserFromGroup(Group groupObj, ApplicationUser reporterObj) {
        try {
            ComponentAccessor.getUserUtil().removeUserFromGroup(groupObj, reporterObj);
        } catch (PermissionException e) {
            e.printStackTrace();
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }
}
