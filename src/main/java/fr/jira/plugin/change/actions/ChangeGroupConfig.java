package fr.jira.plugin.change.actions;

import java.util.Collection;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.upm.api.license.PluginLicenseManager;

import fr.jira.plugin.change.utils.LicenseUtils;

public class ChangeGroupConfig extends JiraWebActionSupport {

	private PluginLicenseManager pluginLicenseManager;

	public ChangeGroupConfig(PluginLicenseManager pluginLicenseManager) {
		this.pluginLicenseManager = pluginLicenseManager;
	}

	boolean isLicenseValid() {
		return LicenseUtils.isLicenseValid(pluginLicenseManager);
	}

	public Collection<ApplicationUser> getUsers() {
		return UserUtils.getUserManager().getAllApplicationUsers();
	}

	public Collection<Group> getGroups() {
		return UserUtils.getUserManager().getGroups();
	}

}
