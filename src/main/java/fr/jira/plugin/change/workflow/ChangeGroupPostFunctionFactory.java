package fr.jira.plugin.change.workflow;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import webwork.action.ActionContext;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;

import fr.jira.plugin.change.utils.LicenseUtils;

/**
 * This is the factory class responsible for dealing with the UI for the
 * post-function. This is typically where you put default values into the
 * velocity context and where you store user input.
 */

public class ChangeGroupPostFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory {

	public static final String GROUP = "group";
	public static final String OPERATION = "operation";
	public static final String USER1 = "user1";
	public static final String USER2 = "user2";
	public static final String FULL = "fullSwitch";

	private PluginLicenseManager pluginLicenseManager;

	public ChangeGroupPostFunctionFactory(PluginLicenseManager pluginLicenseManager) {
		this.pluginLicenseManager = pluginLicenseManager;
	}

	@Override
	protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
		Map<String, String[]> myParams = ActionContext.getParameters();

		// the default message
		velocityParams.put("groups", getGroups());
		velocityParams.put("users", getUsers());
		velocityParams.put("isLicenseValid", LicenseUtils.isLicenseValid(pluginLicenseManager));
	}

	private Collection<ApplicationUser> getUsers() {
		return UserUtils.getUserManager().getAllApplicationUsers();
	}

	private Collection<Group> getGroups() {
		return UserUtils.getUserManager().getGroups();
	}

	@Override
	protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
		getVelocityParamsForInput(velocityParams);
		getVelocityParamsForView(velocityParams, descriptor);
	}

	@Override
	protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
		if (!(descriptor instanceof FunctionDescriptor)) {
			throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
		}

		FunctionDescriptor functionDescriptor = (FunctionDescriptor) descriptor;
		String group = (String) functionDescriptor.getArgs().get(GROUP);
		String operation = (String) functionDescriptor.getArgs().get(OPERATION);
		String user1 = (String) functionDescriptor.getArgs().get(USER1);
		String user2 = (String) functionDescriptor.getArgs().get(USER2);
		String full = (String) functionDescriptor.getArgs().get(FULL);

		velocityParams.put(GROUP, group);
		velocityParams.put(OPERATION, operation);
		velocityParams.put(USER1, user1);
		velocityParams.put(USER2, user2);
		velocityParams.put(FULL, full);

		velocityParams.put("isLicenseValid", LicenseUtils.isLicenseValid(pluginLicenseManager));
	}

	public Map<String, ?> getDescriptorParams(Map<String, Object> formParams) {
		Map params = new HashMap();

		// Process The map
		params.put(GROUP, extractSingleParam(formParams, GROUP));
		params.put(OPERATION, extractSingleParam(formParams, OPERATION));
		params.put(USER1, extractSingleParam(formParams, USER1));
		params.put(USER2, extractSingleParam(formParams, USER2));

		try {
			params.put(FULL, extractSingleParam(formParams, FULL));
		} catch (IllegalArgumentException e) {
			params.put(FULL, null);
		}
		return params;
	}

}