package fr.jira.plugin.change.workflow;

import java.util.Collection;
import java.util.Map;

import com.atlassian.jira.bc.user.ApplicationUserBuilder;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.user.util.UserUtil;
import fr.jira.plugin.change.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.exception.runtime.GroupNotFoundException;
import com.atlassian.crowd.exception.runtime.OperationFailedException;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

import fr.jira.plugin.change.utils.LicenseUtils;

/**
 * This is the post-function class that gets executed at the end of the
 * transition. Any parameters that were saved in your factory class will be
 * available in the transientVars Map.
 */
public class ChangeGroupPostFunction extends AbstractJiraFunctionProvider {

	private static final Logger log = LoggerFactory.getLogger(ChangeGroupPostFunction.class);
	private PluginLicenseManager pluginLicenseManager;

	public static final String GROUP = "group";
	public static final String OPERATION = "operation";
	public static final String USER1 = "user1";
	public static final String USER2 = "user2";
	public static final String FULL = "fullSwitch";

	private GroupManager gm;
	private UserManager um;
	private CrowdService crowdService;

	public ChangeGroupPostFunction(GroupManager gm, UserManager um, CrowdService crowdService, PluginLicenseManager pluginLicenseManager) {
		this.gm = gm;
		this.um = um;
		this.crowdService = crowdService;
		this.pluginLicenseManager = pluginLicenseManager;
	}

	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {

		if (!LicenseUtils.isLicenseValid(pluginLicenseManager)) {
			return;
		}

		MutableIssue issue = getIssue(transientVars);

		String group = (String) args.get(GROUP);
		String operation = (String) args.get(OPERATION);
		String user1 = (String) args.get(USER1);
		String user2 = (String) args.get(USER2);
		String full = (String) args.get(FULL); // APPLY ONLY FOR SWITCH

		boolean isUser1Reporter = (user1 != null && user1.equals("reporter")) ? true : false;
		boolean isUser1Assignee = (user1 != null && user1.equals("assignee")) ? true : false;
		boolean isFull = (full != null && full.equals("true")) ? true : false;

		Group groupObj = (group != null) ? gm.getGroup(group) : null;
		ApplicationUser reporterObj = issue.getReporter();
		ApplicationUser assigneeObj = issue.getAssignee();

		ApplicationUser user1Obj = getProperUserObject(user1, issue);
		ApplicationUser user2Obj = getProperUserObject(user2, issue);

		try {

			if (operation.equals("add")) {

				if (isUser1Reporter) {
					gm.addUserToGroup(reporterObj, groupObj);
				} else if (isUser1Assignee) {
					gm.addUserToGroup(assigneeObj, groupObj);
				} else {
					gm.addUserToGroup(user1Obj, groupObj);
				}

			} else if (operation.equals("remove")) {

				if (isUser1Reporter) {
					Utils.removeUserFromGroup(groupObj, reporterObj);
				} else if (isUser1Assignee) {
					Utils.removeUserFromGroup(groupObj, assigneeObj);
				} else {
					Utils.removeUserFromGroup(groupObj, user1Obj);
				}

			} else if (operation.equals("switch")) {

				

				// Delete all groups
				if (isFull) {
					
					Collection<Group> groupsFromUser1 = gm.getGroupsForUser(user1Obj);
					Collection<Group> groupsFromUser2 = gm.getGroupsForUser(user2Obj);
					
					for (Group g : groupsFromUser1) {
						Utils.removeUserFromGroup(g, user1Obj);
					}
					for (Group g : groupsFromUser2) {
						Utils.removeUserFromGroup(g, user2Obj);
					}
					
					for (Group g : groupsFromUser1) {
						if (!gm.isUserInGroup(user2Obj, g))
							gm.addUserToGroup(user2Obj, g);
					}

					for (Group g : groupsFromUser2) {
						if (!gm.isUserInGroup(user1Obj, g))
							gm.addUserToGroup(user1Obj, g);
					}
				} else {
					Utils.removeUserFromGroup(groupObj, user1Obj);
					gm.addUserToGroup(user2Obj, groupObj);
				}

				


			}

		} catch (GroupNotFoundException | UserNotFoundException | OperationNotPermittedException | OperationFailedException
				| com.atlassian.crowd.exception.GroupNotFoundException | com.atlassian.crowd.exception.OperationFailedException e) {
			e.printStackTrace();
		}
	}

	private ApplicationUser getProperUserObject(String user12, MutableIssue issue) {
		if (user12.equals("reporter"))
			return issue.getReporter();
		else if (user12.equals("assignee")) {
			return issue.getAssignee();
		} else {
			return um.getUserByKey(user12);
		}
	}
}