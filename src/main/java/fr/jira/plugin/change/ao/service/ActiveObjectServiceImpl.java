package fr.jira.plugin.change.ao.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.issue.CustomFieldManager;

public class ActiveObjectServiceImpl implements ActiveObjectService {

	private final ActiveObjects ao;
	private CustomFieldManager customFieldManager;

	public ActiveObjectServiceImpl(ActiveObjects ao, CustomFieldManager customFieldManager) {
		this.ao = ao;
		this.customFieldManager = customFieldManager;
	}

}
