package fr.jira.plugin.change.ao;

import net.java.ao.Entity;

public interface Events extends Entity {

	String getCascading();

	void setCascading(String cascading);

	Long getLevel1();

	void setLevel1(Long level1);

	Long getLevel2();

	void setLevel2(Long level2);

	Long getContext();

	void setContext(Long context);

	String getRelated();

	void setRelated(String related);

}
